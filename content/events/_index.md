---
title: Events
menu: main
outputs:
- HTML
- Calendar
---

This is the list of the events. You can also subscribe to the ics calendar:

<a href="index.ics"><button type="button" class="btn btn-primary">Subscribe</button></a>