---
title: "Universitätsgesellschaft Paderborn"
link: https://www.uni-paderborn.de/universitaet/universitaetsgesellschaft-paderborn/
weight: 2
resources:
- name: logo
  src: ugpb.png
---

The Universitätsgesellschaft Paderborn e.V. sponsored our hoodies.
